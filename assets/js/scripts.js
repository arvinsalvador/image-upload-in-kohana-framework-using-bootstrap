	$(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
			var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		    	console.log(log);
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		            $('#img-upload').css('display','inline');
		            $('#img-remove').css('display','inline');
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$('#add-rec').click(function(){
			$('#modal-title').html('Upload Image');
			$('#img_id').val('');
			$('#img-remove').trigger('click');
			$('#img_title').val('');
			$('#img_filename').val('');
		});

		$("#uploadimage").on('submit', function(e){
			URL_ACT = $('#action_url').val();
			$('#upload-loader').css('display','inline');
			e.preventDefault();
			$.ajax({
				url 	: URL_ACT,
				type 	: "POST",
				data 	: new FormData(this),
				contentType	: false,
				cache	: false,
				processData	: false,

				success : function(data){
					console.log(data);
					if(data.substring(0, 5) == 'Error'){
						$('#error-message-p').text(data);
						$('#new-error').css('display','block');
						setTimeout(function(){
							$('#new-error').css('display','none');
						},2500);
					}else{
						$('#success-message-p').text('Successfully Saved!');
						$('#new-success').css('display','block');
						setTimeout(function(){
							$('#new-success').css('display','none');
							$('#myModal').modal('hide');
						}, 2500);
					}
					$("#table-containers").load(location.href + " #table-containers");
					$('#upload-loader').css('display','none');
				},
				error : function(data){
					$('#error-message-p').text(data);
					$('#new-error').css('display','block');
					$('#upload-loader').css('display','none');
					setTimeout(function(){
						$('#new-error').css('display','none');
					},2500);
				}
			});
		});

		$("#act_deletes").click(function(e){
			var URL_ACT = 'file/delete';
			var idn = $('#img_idn').val();
			$('#upload-loader-update').css('display','inline');
			e.preventDefault();
			$.ajax({
				url 	: URL_ACT,
				type 	: "POST",
				data 	: { id : idn },

				success : function(data){
					console.log(data);
					if(data.substring(0, 5) == 'Error'){
						$('#error-message-p-update').text(data);
						$('#update-error').css('display','block');
					}else{
						$('#success-message-p-update').text('Successfully Deleted!');
						$('#update-success').css('display','block');
					}
					$("#table-containers").load(location.href + " #table-containers");
					$('#upload-loader-update').css('display','none');
					setTimeout(function(){
					    $('#update-error').css('display','none');
					    $('#update-success').css('display','none');
					    $('#delete-modal').modal('hide');
					 }, 2000);
				},
				error : function(data){
					$('#error-message-p-update').text(data);
					$('#update-error').css('display','block');
					$('#upload-loader-update').css('display','none');
					setTimeout(function(){
						$('#new-error').css('display','none');
					},2500);
				}
			});
		});

		$("#img_image").change(function(){
		    readURL(this);
		}); 	

		$("#img-remove").click(function(){
			$('#img-upload').css('display','none');
			$(this).css('display','none');
		});
	});

	function process_update(idn){
		URL_ACT = 'file/getrecords';
		$.ajax({
			url 	: URL_ACT,
			type 	: "POST",
			data 	: {
				id : idn
			},
			success : function(data){
				console.log(data);
				var results = $.parseJSON(data);
				console.log(results[0].up_TITLE);
				$('#new-error').css('display','none');
				$('#new-success').css('display','none');
				$('#myModal').modal('show');
				$('#modal-title').html('Update Image');
				$('#img_id').val(idn);
				$('#img_title').val(results[0].up_TITLE);
				$('#img_filename').val(results[0].up_FILENAME);
				$('#img-upload').css('display','inline');
	            $('#img-remove').css('display','inline');
				$('#img-upload').attr('src', 'assets/images/' + results[0].up_FILENAME);

			},
			error : function(data){
				console.log(data);
			}
		});
	}

	function process_delete(idn){
		$('#img_idn').val(idn);
		$('#delete-modal').modal('show');
	}