<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller_Template {

	public $template = 'page_index';

	public function action_index()
	{
		$records = ORM::factory('Records')->find_all();

		if(isset($upload_records[0])){
			$this->template->content = View::factory('page_records')->bind('records', $records);
		}else{
			$upload_records = array();
			$this->template->content = View::factory('page_records')->bind('records', $records);
		}
	}

} 
