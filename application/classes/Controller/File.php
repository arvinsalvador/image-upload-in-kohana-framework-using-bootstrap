<?php defined('SYSPATH') or die('No direct script access.');

class Controller_File extends Controller {

	public function action_save()
	{
		if($this->request->is_ajax()){

			$ID 		= $_POST['img_id'];

			$FILE_IMAGE = $_FILES['img_image'];
			$FILE_NAME 	= $_FILES['img_image']['name'];
			$FILE_TITLE = $_POST['img_title'];
			$FILE_TEMP	= $_FILES['img_image']['tmp_name'];

			$FILE_EXT 	= strtolower(end(explode('.',$FILE_NAME)));
			$FILE_SIZE	= $_FILES['img_image']['size'];

			$EXTENSIONS = array('jpeg', 'jpg', 'png', 'gif');

			sleep(1.5);

			if(!$FILE_IMAGE && !$ID){
				echo 'Error: Please select an image before you save.';
			}else{
				if(in_array($FILE_EXT, $EXTENSIONS) === false && !$ID){
					echo 'Error: Please select a valid image file. Only .jpg, .jpeg and .png are acceptible';
				}else{
					if($FILE_SIZE > 2097152 && !$ID){
						//if image exceeds 2MB
						echo 'Error: File must be less than 2MB in size.';
					}else{

						if($ID){
							$POSTS = ORM::factory('Records', $ID);
						}else{
							$POSTS = ORM::factory('Records');
						}
						$POSTS->up_TITLE = $FILE_TITLE;
						$POSTS->up_FILENAME = $_POST['img_filename'];
						$POSTS->up_PATH = 'assets/images/';
						$POSTS->up_DATE = time();
						$POSTS->save();

						if($FILE_IMAGE){
							move_uploaded_file($FILE_TEMP, "assets/images/" . $FILE_NAME);
						}
						$Req = Request::factory('/index/index');
						$Req->execute();
						echo 'Success';
					}
				}
			}
		}else{
			echo 'Error: There was an error upon receiving request.';
		}
	}

	public function action_getrecords()
	{
		if($this->request->is_ajax()){
			$ID = $_POST['id'];
			$this->auto_render = false;
			$this->is_ajax = TRUE;
            header('content-type: application/json');
            $RECORDS = json_encode(array_map(create_function('$obj', 'return $obj->as_array();'), ORM::factory('Records')->where('up_ID', '=', $ID)->find_all()->as_array()));
            echo $RECORDS;

		}else{
			echo 'Error: Failed to retrieve records';
		}
	}

	public function action_delete()
	{
		if($this->request->is_ajax()){

			$ID 	= $_POST['id'];
			$REQS 	= ORM::factory('Records', $ID)->delete();
			$REQ 	= Request::factory('/index/index');
			$REQ->execute();
			sleep(1.5);
			echo 'Success';
		}else{
			echo 'Error: Unable to process request';
		}
	}

} 
