<html>
	<head>
		<title>Image Upload - Kohana Framework</title>
		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  		<script src="https://use.fontawesome.com/23356343b4.js"></script>
  		<script src="assets/js/scripts.js"></script>
		<style>
			body {
				padding:50px;
			}
			.btn-file {
			    position: relative;
			    overflow: hidden;
			}
			.btn-file input[type=file] {
			    position: absolute;
			    top: 0;
			    right: 0;
			    min-width: 100%;
			    min-height: 100%;
			    font-size: 100px;
			    text-align: right;
			    filter: alpha(opacity=0);
			    opacity: 0;
			    outline: none;
			    background: white;
			    cursor: inherit;
			    display: block;
			}

			#img-upload{
			    width: 50%;
			    padding:20px;
			    display:none;
			}

			#img-remove, #upload-loader, #upload-loader-update, #update-success, #update-error, #new-success, #new-error {
				display:none;
			}
		</style>
	</head>
	<body>
		<header>
			
		</header>
		<main>
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<p>
							<button type="button" id="add-rec" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">ADD</button>

							<!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog">
							  	<div class="modal-dialog">
								    <!-- Modal content-->
								    <div class="modal-content">
								    	<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
								    		<input type="hidden" id="action_url" value="<?php echo URL::base() . 'file/save'; ?>" />
								    		<input type="hidden" id="img_id" name="img_id" value="" />
									      	<div class="modal-header">
									        	<button type="button" class="close" data-dismiss="modal">&times;</button>
									        	<h4 class="modal-title" id="modal-title"></h4>
									      	</div>
									      	<div class="modal-body">
								        		<div id="new-success" class="alert alert-success alert-dismissable">
								        			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												  	<p id="success-message-p"></p>
												</div>
												<div id="new-error" class="alert alert-danger alert-dismissable">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												  	<p id="error-message-p"></p>
												</div>
							        			<div class="form-group text-center">	
							        				<img id="img-upload" class="text-center"/>
							        				<br />
							        				<button id="img-remove" type="button" class="btn btn-danger btn-xs">Remove Preview</button>
							        			</div>
							        			<div class="form-group">
							        				
											        <div class="input-group">
											            <span class="input-group-btn">
											                <span class="btn btn-default btn-file">
											                    Browse… <input type="file" id="img_image" name="img_image">
											                </span>
											            </span>
											            <input type="text" id="img_filename" name="img_filename" class="form-control" readonly>
											        </div>
											        
							        			</div>
							        			<div class="form-group">
							        				<label for="img_title">Title:</label>
							        				<input type="text" name="img_title" id="img_title" class="form-control" />
							        			</div>
									      	</div>
									      	<div class="modal-footer">
									      		<img id="upload-loader" src="assets/images/imgs/dereload.gif" width="30" />
									      		<input type="submit" class="btn btn-info" id="act_save" value="Save" />
									        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									      	</div>
									    </form>
								    </div>
							  	</div>
							</div>
							<div id="delete-modal" class="modal fade" role="dialog">
							  	<div class="modal-dialog">
								    <!-- Modal content-->
								    <div class="modal-content">
								    	
							    		<input type="hidden" id="action_urls" value="<?php echo URL::base() . 'file/delete'; ?>" />
							    		<input type="hidden" id="img_idn" name="img_idn" value="" />
								      	<div class="modal-header">
								        	<button type="button" class="close" data-dismiss="modal">&times;</button>
								        	<h4 class="modal-title">Delete Image</h4>
								      	</div>
								      	<div class="modal-body">
							        		<div id="update-success" class="alert alert-success alert-dismissable">
							        			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											  	<p id="success-message-p-update"></p>
											</div>
											<div id="update-error" class="alert alert-danger alert-dismissable">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											  	<p id="error-message-p-update"></p>
											</div>
						        			<p>
						        				<h3>Are you sure you want to delete this record?</h3>
						        			</p>
								      	</div>
								      	<div class="modal-footer">
								      		<img id="upload-loader-update" src="assets/images/imgs/dereload.gif" width="30" />
								      		<button class="btn btn-info" id="act_deletes">Yes</button>
								        	<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
								      	</div>
								    </div>
							  	</div>
							</div>
						</p>
						<div class="table-responsive" id="table-containers">
							<?=$content?>
						</div>
					</div>
				</div>
			</div>
		</main>
		<footer>
			
		</footer>
	
	</body>
</html>