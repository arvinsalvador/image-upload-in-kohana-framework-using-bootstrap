							<table class="table">
								<thead>
									<th width="250" class="text-center">Title</th>
									<th width="20" class="text-center">Thumbnail</th>
									<th width="200" class="text-center">Filename</th>
									<th width="150" class="text-center">Date Added</th>
									<th width="150" class="text-center">Actions</th>
								</thead>
								<tbody>
									<?php
										foreach($records as $R){
									?>
											<tr>
												<td><?=$R->up_TITLE?></td>
												<td class="text-center"><img src="<?=$R->up_PATH . $R->up_FILENAME?>" width="20%" /></td>
												<td><?=$R->up_FILENAME?></td>
												<td class="text-center"><?= date('Y-m-d', $R->up_DATE) ?></td>
												<td class="text-center">
													<a class="btn btn-default" onClick="process_update(<?=$R->up_ID ?>)" href="#"><i class="fa fa-edit fa-lg"></i> </a>
													<a class="btn btn-default" onClick="process_delete(<?=$R->up_ID ?>)" href="#"><i class="fa fa-trash fa-lg"></i> </a>
												</td>
											</tr>
									<?php
										}
									?>
								</tbody>
							</table>